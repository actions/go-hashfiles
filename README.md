# go-hashfiles

This action is to compute the SHA256 hash of specified files.

The hash function is based on [nektos/act](https://github.com/nektos/act/blob/ac5dd8feb876d37ae483376a137c57383577dace/pkg/exprparser/functions.go#L183). Thanks!

**NOTE:** This action is written in Go. Please setup the Go environment before running this action or use a runner with Go environment installed.

## Usage

``` yml
- uses: actions/go-hashfiles@v0.0.1
  with:
    # The working dir for the action.
    # Default: ${{ github.workspace }}
    workdir: ''

    # The patterns used to match files.
    # Multiple patterns should be seperated by `\n`
    patterns: ''
```

## Output

|Output Item|Description|
|---|---|
|hash|The computed hash result|
|matched-files|The files matched by the patterns|

## Example
``` yml
# Setup the Go environment. This step can be skipped if Go has been installed.
- uses: actions/setup-go@v3
  with:
    go-version: '1.20'

- uses: actions/go-hashfiles@v0.0.1
  id: get-hash
  with: 
    patterns: |-
      go.sum
      ./admin/*
      **/package-lock.json

- name: Echo hash
  run: echo ${{ steps.get-hash.outputs.hash }}
```
